using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectLimter : MonoBehaviour
{
    public string _CompareTag;
    // Start is called before the first frame update
    private void OnCollisionEnter2D(Collision2D collision)
    {
        print(collision.gameObject.tag);
        if(collision.gameObject.CompareTag(_CompareTag))
            Destroy(collision.gameObject);
    }
}
