using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotDogBurstSFX : MonoBehaviour
{
    AudioSource audioSource;
    public static HotDogBurstSFX instance;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        audioSource = GetComponent<AudioSource>();    
    }

    // Update is called once per frame
    public void Play() { 
        audioSource.Play();
    }
}
