using System.Collections;
using System.Collections.Generic;
using System.Xml.Schema;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemySpawner : MonoBehaviour
{
    public static EnemySpawner Instance;

    int _killedEnemies = 0;
    readonly int EnemyDifficultyIncrementLimit = 10;
    int notKilled = 0;
    static bool hasLost = false;

    public GameObject[] enemies; 
    public Transform[] SpawnPoints;
    float spawnInterval = 3;
    float curTime = 0;
    float probSpecialEnemies = 0;

    // Start is called before the first frame update
    private void Start()
    {
        hasLost = false;
        Instance = this;
    }
    // Update is called once per frame
    void Update()
    {
        curTime += Time.deltaTime;
        if (curTime > spawnInterval) {
            SpawnNew();
            curTime = 0;
        }
    }

    void SpawnNew() {
        var trans = SpawnPoints[Random.Range(0, SpawnPoints.Length)];
        var prob = Random.value;
        GameObject enemy;
        if (prob < probSpecialEnemies)
        {
            enemy = enemies[Random.Range(1, 3)];
        }
        else {
            enemy = enemies[0];
        }
        Instantiate(enemy, trans.position, trans.rotation);
    }

    public void IncrementKills() {
        _killedEnemies++;
        if (_killedEnemies % EnemyDifficultyIncrementLimit == 0) {
            probSpecialEnemies = Mathf.Clamp(probSpecialEnemies + 0.05f, 0f, 0.5f);
            spawnInterval = Mathf.Clamp(spawnInterval - 0.15f, 0.15f, 3);
        }
    }

    public void IncrementNotKilled() {
        notKilled++;
        if (notKilled > 4) {
            hasLost = true;
            SceneManager.LoadScene(0, LoadSceneMode.Single);
        }
    }

    public static bool HasLost() => hasLost;

    public void NotKilled() { 
        
    }
}
