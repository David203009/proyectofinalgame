using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ad : MonoBehaviour
{
    Vector2 vel = new Vector2(0, -2.5f);


    // Update is called once per frame
    void Update()
    {
        transform.Translate(Time.deltaTime * vel);
    }

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonUp(0)) {
            MoneyCounter.instance.IncrementAmount(30);
            Destroy(gameObject);
        }
    }
}
