using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HappySodaBullet : MonoBehaviour
{
    readonly Vector2 xspeed = Vector2.right * 5;
    Collider2D collider;
    Animator animator;
    public Transform Limit;

    bool isExploding = false;
    // Start is called before the first frame update
    void Start()
    {
        Limit = GameObject.FindGameObjectWithTag("BulletLimit").GetComponent<Transform>();
        animator = GetComponent<Animator>();
        collider = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if(isExploding) return;
        transform.Translate(Time.deltaTime * xspeed);
        CheckLimit();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.CompareTag("Enemy"))
        {
            BulletCollisionSFX.instance.Play();
            collision.gameObject.GetComponent<Enemy>().Damage(5);
            if (!isExploding) {
                StartCoroutine(Explode());
            }
            
        } else if (collision.gameObject.CompareTag("Bullet")){
            Physics2D.IgnoreCollision(collider, collision.collider);
        }
    }

    IEnumerator Explode() {
        SplashBulletSFX.instance.Play();

        isExploding = true;
        animator.SetBool("Explode", true);

        float sc = 4f;
        Vector3 scaleStep = new Vector3(5.5f/sc, 5.5f/sc);
        float timeStep = 0.917f*0.9f / sc;

        for (;sc >= 1; sc--)
        {
            yield return new WaitForSeconds(timeStep);
            transform.localScale += scaleStep;
        }
        Destroy(gameObject);
    }

    void CheckLimit()
    {
        if (transform.position.x > Limit.position.x)
        {
            Destroy(gameObject);
        }
    }
}
