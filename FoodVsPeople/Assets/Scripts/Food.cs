using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour
{

    bool followMouse = true;
    Cell currentCell = null;
    bool onCell = false;
    public bool isHotDog = false;
    bool hasHotDogStarted = false;
    public Animator hotDogAnimator;
    public int BulletCount = 0;

    //SFX
    public AudioClip eatingClip;

    public float shootInterval = 1.5f;
    float curShootTime = 0;
    public GameObject bullet;
    public int cost;
    float health = 10;

    // Start is called before the first frame update
    void Start()
    {
        if (isHotDog) hotDogAnimator = GetComponent<Animator>();
        FoodSelector.currentFoodSelection = this;
    }

    // Update is called once per frame
    void Update()
    {
        curShootTime += Time.deltaTime;
        if (followMouse) {
            FollowMouse();
            return;
        };
        if (isHotDog) {
            if (!hasHotDogStarted) StartCoroutine(HotDog());
            return;
        };
        if (onCell) {
            Shoot();
        };

    }

    void FollowMouse() {
        var vecPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        vecPos.z = 0;
        transform.position = vecPos;
    }

    public void SetPosition(Vector3 _, Cell cell) {
        if (currentCell != null) {
            currentCell.Disoccupy();
        }
        FoodSelector.currentFoodSelection = null;
        onCell = true;
        currentCell = cell;
        transform.position = cell.transform.position;
    }

    public void SetCell(Cell cell) {
        currentCell = cell;
    }

    public void StopMouseFollow() {
        followMouse = false;
    }

    private void Shoot() {
        if (curShootTime > shootInterval) {
            curShootTime = 0;
            Instantiate(bullet, transform.position, transform.rotation);
            BulletCount--;
            if (BulletCount <= 0) {
                Destroy(gameObject);
            }
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Food")) {
            print(collision.gameObject.tag);
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), collision.collider);
        }
    }

    public bool OnCell() => onCell;

    public void RecieveDamage(float amount) {
        health -= amount;
        if (health <= 0) {
            Destroy(gameObject);
        }
    }

    IEnumerator HotDog() {
        hotDogAnimator.SetBool("BeginShoot", true);
        HotDogBurstSFX.instance.Play();

        hasHotDogStarted = true;

        float dur = 1.083f;
        float step = dur / 13;

        for (float i = 0; i < dur - step*3; i+=step)
        {
            yield return new WaitForSeconds(step);
            Instantiate(bullet, transform.position, transform.rotation);
        }

        Destroy(gameObject);
    }
    private void OnDestroy()
    {
        currentCell?.Disoccupy();
    }
}
