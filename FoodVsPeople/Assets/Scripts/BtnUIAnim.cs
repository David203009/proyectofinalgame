using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BtnUIAnim : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Color baseColor;
    public Color hoverColor;
    
    public Text innerText;
    // Start is called before the first frame update
    private void Start()
    {
        baseColor = new Color(0.5f, 0.5f, 0.5f);
        hoverColor = new Color(1, 1, 1);

        innerText.color = baseColor;
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        innerText.color = hoverColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        innerText.color = baseColor;
    }
}
