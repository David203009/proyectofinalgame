using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FoodSelector : MonoBehaviour
{
    public Button[] Buttons;
    public GameObject[] Foods;
    public int[] FoodCosts;
    public static Food currentFoodSelection = null;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < Buttons.Length; i++)
        {
            int c = i;
            Buttons[i].onClick.AddListener(() => {
                if (FoodSelector.currentFoodSelection != null) { 
                    Destroy(FoodSelector.currentFoodSelection);
                }
                if (FoodCosts[c] > MoneyCounter.instance.Amount)
                {
                    return;
                }
                MoneyCounter.instance.DecrementAmount(FoodCosts[c]);
                Instantiate(Foods[c], Buttons[c].transform.position, Buttons[c].transform.rotation);
            });
        }
    }

    // Update is called once per frame
}
