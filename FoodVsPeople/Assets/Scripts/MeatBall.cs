using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeatBall : MonoBehaviour
{
    bool followMouse;
    Animator animator;

    public int Cost;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        followMouse = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(followMouse) FollowMouse();
        if (Input.GetMouseButtonUp(0) && followMouse) {
            followMouse = false;
            StartCoroutine(Explode());
        }
    }

    void FollowMouse()
    {
        var vecPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        vecPos.z = 0;
        transform.position = vecPos;
    }

    IEnumerator Explode() {

        MeatBallExplosionSFX.instance.Play();
        animator.SetBool("Explode", true);

        var l = 1.200f;
        var stepCount = 4;
        int sch = stepCount / 2;

        var scaleStep = new Vector3(5f/stepCount, 5f/stepCount);
        var timeStep = l / stepCount;

        for (;stepCount>=1; stepCount--)
        {
            if(stepCount == sch) gameObject.AddComponent<CircleCollider2D>();
            transform.localScale += scaleStep;
            yield return new WaitForSeconds(timeStep);
        }

        MeatBallExplosionSFX.instance.SFXReset();


        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Food"))
        {
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), collision.collider);
        }
        else if (collision.gameObject.CompareTag("Enemy")) { 
            Destroy(collision.gameObject);
        }
    }
}
