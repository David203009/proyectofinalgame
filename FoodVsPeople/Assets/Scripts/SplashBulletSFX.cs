using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashBulletSFX : MonoBehaviour
{
    public static SplashBulletSFX instance;
    public AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    public void Play() { 
        audioSource.Play();
    }
}
