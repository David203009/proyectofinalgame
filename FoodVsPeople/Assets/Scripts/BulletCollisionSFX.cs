using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCollisionSFX : MonoBehaviour
{
    public static BulletCollisionSFX instance;
    AudioSource audioSource;
    public AudioClip OnHitClip;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = OnHitClip;
    }

    // Update is called once per frame
    public void Play()
    {
        audioSource.Play();
    }
}
