using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyCounter : MonoBehaviour
{
    public int Amount { get; private set; } = 0;    
    public static MoneyCounter instance;
    public Text moneyText;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        UpdateText();
    }

    // Update is called once per frame
    public void IncrementAmount(int amount) {
        Amount += amount;
        UpdateText();
        
    }

    void UpdateText()
    {
        moneyText.text = $"Money: {Amount}";
    }

    public void DecrementAmount(int amount) {
        Amount = Mathf.Max(Amount - amount, 0);
        UpdateText();
    }
}
