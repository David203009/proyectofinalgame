using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FatEnemy : MonoBehaviour
{

    Vector2 velocity;
    int health = 20;
    // Start is called before the first frame update
    void Start()
    {
        velocity = Vector2.right * - 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Time.deltaTime * velocity);
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Food")) { 
            
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Food"))
        {

        }
    }

    void Damage(int amount = 1)
    {
        health -= amount;
        if (health <= 0)
        {
            Destroy(gameObject);
        }
    }


}
