using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EatingSFXPlayer : MonoBehaviour
{
    public static EatingSFXPlayer instance;
    public AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    public void PlayEatingClip(AudioClip clip) { 
        audioSource.clip = clip;
        audioSource.Play();
    }
}
