using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    Vector3 velocity;
    public float speed;
    Animator animator;
    bool hasc = false;
    public int health = 10;
    public float foodDamage;
    public Transform Limit;
    Food curFood;

    //SoundClipVars
    bool isEating;
    float curClipTime = 0.4f;
    float maxClipTime = 0.4f;


    // Start is called before the first frame update
    void Start()
    {
        Limit = GameObject.FindGameObjectWithTag("EnemyLimit").GetComponent<Transform>();
        velocity = Vector3.right * -speed;
        animator = GetComponent<Animator>();
        //Vector3 angs = transform.rotation.eulerAngles;
        //angs.y += 180;
        //transform.rotation = Quaternion.Euler(angs);
    }

    // Update is called once per frame
    void Update()
    {
        if (!hasc)
        {
            transform.Translate(Time.deltaTime * velocity);
            CheckLimit();
        }
        else {
            DoEatingSoundClip();
            curFood.RecieveDamage(foodDamage * Time.deltaTime);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Food")) {
            if (hasc) return;
            curFood = collision.gameObject.GetComponent<Food>();
            SetEatAnim(true);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Food"))
        {
            curClipTime = 0;
            curFood = null;
            SetEatAnim(false);
        }
    }

    void SetEatAnim(bool val) {
        hasc = val;
        animator.SetBool("HasCollided", hasc);
    }

    public void Damage(int amount = 1) {
        health -= amount;
        if (health <= 0) {
            Destroy(gameObject);
        }
    }

    void CheckLimit() {
        if (transform.position.x < Limit.position.x) {
            EnemySpawner.Instance.IncrementNotKilled();
            Destroy(gameObject);
        }
    }

    void DoEatingSoundClip() { 
        curClipTime += Time.deltaTime;
        if(curClipTime > maxClipTime) {
            curClipTime = 0;
            EatingSFXPlayer.instance.PlayEatingClip(curFood.eatingClip);
        }
    }
    private void OnDestroy()
    {
        EnemySpawner.Instance.IncrementKills();
    }
}
