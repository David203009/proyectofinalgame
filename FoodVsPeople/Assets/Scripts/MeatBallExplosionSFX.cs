using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeatBallExplosionSFX : MonoBehaviour
{
    public static MeatBallExplosionSFX instance;
    AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        audioSource = GetComponent<AudioSource>();  
    }

    // Update is called once per frame
    public void Play() {
        audioSource.Play();
    }

    public void SFXReset()
    {
        audioSource.Stop();
    }
}
