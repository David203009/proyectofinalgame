using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Money : MonoBehaviour
{
    Vector2 speed = new Vector2(0, -2.5f);
    public bool isAd = false;
    // Start is called before the first frame update
    //void Start()
    //{
        
    //}

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Time.deltaTime * speed);
    }
    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0)) {
            MoneyCounter.instance.IncrementAmount(isAd ? 30 : 5);
            Destroy(gameObject);
        }
    }
}
