using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    readonly Vector2 xspeed = Vector2.right * 3;
    public Transform Limit;
    public int DamageAmount = 1;
    // Start is called before the first frame update
    private void Start()
    {
        Limit = GameObject.FindGameObjectWithTag("BulletLimit").GetComponent<Transform>();

    }
    // Update is called once per frame
    void Update()
    {
        transform.Translate(Time.deltaTime * xspeed);
        CheckLimit();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy")) {
            BulletCollisionSFX.instance.Play();
            collision.gameObject.GetComponent<Enemy>().Damage(DamageAmount);
            Destroy(gameObject);
        }
    }

    void CheckLimit() {
        if (transform.position.x > Limit.position.x) {
            Destroy(gameObject);
        }
    }
}