using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneySpawner : MonoBehaviour
{
    public Transform rightPoint;
    public Transform leftPoint;
    public GameObject money;
    public GameObject[] ads;
    float spawnInterval;
    float curTime;

    float adInterval;
    float adCurTime = 0;
    // Start is called before the first frame update
    void Start()
    {
        adInterval = RandomAdInterval();
        SetRandomInterval();
    }

    float RandomAdInterval() => Random.Range(10f, 20f);

    //Update is called once per frame
    void Update()
    {
        curTime += Time.deltaTime;
        adCurTime += Time.deltaTime;
        if (curTime > spawnInterval)
        {
            Instantiate(money, new Vector2(RandomX(), rightPoint.position.y), Quaternion.identity);
            SetRandomInterval();
        }

        if (adCurTime > adInterval) { 
            adInterval =RandomAdInterval();
            adCurTime = 0;
            SpawnAd();
        }
    }
    void SetRandomInterval() {
        curTime = 0;
        spawnInterval = Random.Range(0.5f, 1.5f);
    }
    float RandomX() { 
        return Random.Range(leftPoint.position.x, rightPoint.position.x);
    }

    void SpawnAd() {
        var ad = ads[Random.Range(0, ads.Length)];
        Instantiate(ad, new Vector2(RandomX(), rightPoint.position.y), Quaternion.identity);
    }
}
