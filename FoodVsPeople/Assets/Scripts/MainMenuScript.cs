using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour
{
    public Button playBtn;
    public Button exitBtn;
    public Text lostText;
    // Start is called before the first frame update
    void Start()
    {
        playBtn.onClick.AddListener(() => SceneManager.LoadScene(1, LoadSceneMode.Single));
        exitBtn.onClick.AddListener(() => Application.Quit());
        if (EnemySpawner.HasLost()) {
            lostText.text = "YOU LOST!!!";
        }
        else
        {
            lostText.text = "";
        }
    }

}
