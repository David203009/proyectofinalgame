using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
    // Start is called before the first frame update

    public int Row;

    bool isOccupied = false;

    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    //print("Has collided");
    //}

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!collision.gameObject.CompareTag("Food")) return;
        if (isOccupied) return;

        MeatBall m;
        if (collision.gameObject.TryGetComponent<MeatBall>(out m)) {
            return;
        }

        Food f = collision.gameObject.GetComponent<Food>();
        if (Input.GetMouseButtonUp(0))
        {
            isOccupied = true;
            f.StopMouseFollow();
            f.SetPosition(transform.position, this);
        }
        else {
            f.SetCell(this);
        }
    }

    public void Disoccupy() {
        isOccupied = false;
    }
}
