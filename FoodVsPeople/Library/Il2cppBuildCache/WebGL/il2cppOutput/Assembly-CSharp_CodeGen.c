﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Ad::Update()
extern void Ad_Update_m0737678DBC5B201E41569F013BD1968FEFD4105C (void);
// 0x00000002 System.Void Ad::OnMouseOver()
extern void Ad_OnMouseOver_mB67E9B63528F82639A78327B5B8AC41CD822FC2A (void);
// 0x00000003 System.Void Ad::.ctor()
extern void Ad__ctor_m382BABF025DD59677C46BE975AD2EFFFFB17D786 (void);
// 0x00000004 System.Void BtnUIAnim::Start()
extern void BtnUIAnim_Start_mE630C13F932121054C3FCAB33C64845147A9D24F (void);
// 0x00000005 System.Void BtnUIAnim::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void BtnUIAnim_OnPointerEnter_m5031E9AD336BBD24E0A326D5C22190F7FF8FA60C (void);
// 0x00000006 System.Void BtnUIAnim::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void BtnUIAnim_OnPointerExit_m0957D203470A7C1892C29D0D00FFC621F2A62B48 (void);
// 0x00000007 System.Void BtnUIAnim::.ctor()
extern void BtnUIAnim__ctor_m57A995316CBBA22870E9B2EFDDC8207CE2EF2DC7 (void);
// 0x00000008 System.Void Bullet::Start()
extern void Bullet_Start_m58181B46F80FE1ABECE1AFF455C253B51B7EB0E4 (void);
// 0x00000009 System.Void Bullet::Update()
extern void Bullet_Update_mB82408CA535D8533168045E7C0321090448B596B (void);
// 0x0000000A System.Void Bullet::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Bullet_OnCollisionEnter2D_m4CC80D190EE3A6B6E2EDA524A121942D0285D0A9 (void);
// 0x0000000B System.Void Bullet::CheckLimit()
extern void Bullet_CheckLimit_mC03B03994B3FD61DF42471BB4514A70A7288228D (void);
// 0x0000000C System.Void Bullet::.ctor()
extern void Bullet__ctor_mC7D931FE508342F413FBA79525A4819D4114B3EC (void);
// 0x0000000D System.Void BulletCollisionSFX::Start()
extern void BulletCollisionSFX_Start_m0F3D065B83D2B3449725A1A0C0F8C62B066C597D (void);
// 0x0000000E System.Void BulletCollisionSFX::Play()
extern void BulletCollisionSFX_Play_mCAB4D2BE4E57B52999154A100B16DFDCC033E84D (void);
// 0x0000000F System.Void BulletCollisionSFX::.ctor()
extern void BulletCollisionSFX__ctor_mF5347294466952AEFD874945638D93B74D93C1CD (void);
// 0x00000010 System.Void Cell::OnTriggerStay2D(UnityEngine.Collider2D)
extern void Cell_OnTriggerStay2D_m6CDC37C2ABD61860B2D4539AA8031DA05946DEE7 (void);
// 0x00000011 System.Void Cell::Disoccupy()
extern void Cell_Disoccupy_mD6FA209271B12606A6AD2A562EBA1EE8D2383C24 (void);
// 0x00000012 System.Void Cell::.ctor()
extern void Cell__ctor_m8A64B8FBB74A0ECF5EE0871E80CD28E644CBAAF7 (void);
// 0x00000013 System.Void EatingSFXPlayer::Start()
extern void EatingSFXPlayer_Start_m82E2B0033416D89F79771443DA7CFB28D080E01E (void);
// 0x00000014 System.Void EatingSFXPlayer::PlayEatingClip(UnityEngine.AudioClip)
extern void EatingSFXPlayer_PlayEatingClip_m201E93CA7C16BB956C05CAA3A1C9CA3E710FF576 (void);
// 0x00000015 System.Void EatingSFXPlayer::.ctor()
extern void EatingSFXPlayer__ctor_m7D428A4DD85451C5ACC663C54F9585B5F09A97A9 (void);
// 0x00000016 System.Void Enemy::Start()
extern void Enemy_Start_m9FA35B427F2B9FDFD390E9812C2556775C62CB02 (void);
// 0x00000017 System.Void Enemy::Update()
extern void Enemy_Update_mA01EE7AF5D3B97687752E9D22BECB4A3E13F8FD2 (void);
// 0x00000018 System.Void Enemy::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Enemy_OnCollisionEnter2D_m47EAF0D1D60EC5BF3953975EA6BF15189DF82A12 (void);
// 0x00000019 System.Void Enemy::OnCollisionExit2D(UnityEngine.Collision2D)
extern void Enemy_OnCollisionExit2D_m90E6DC91A08B3861C3E2A6440553466DAE158E1B (void);
// 0x0000001A System.Void Enemy::SetEatAnim(System.Boolean)
extern void Enemy_SetEatAnim_m31342088BDB7E2AF7C9A010A5E92284034E1228B (void);
// 0x0000001B System.Void Enemy::Damage(System.Int32)
extern void Enemy_Damage_m56D6962DD19E23B5E086C2F6CB4D097E1CA61F19 (void);
// 0x0000001C System.Void Enemy::CheckLimit()
extern void Enemy_CheckLimit_m74A309FB85B77A5407AB2698FE153A720F5B5BEE (void);
// 0x0000001D System.Void Enemy::DoEatingSoundClip()
extern void Enemy_DoEatingSoundClip_mB1FCECC6982E1CE7B331C92C6EE9B3DED9FBC53D (void);
// 0x0000001E System.Void Enemy::OnDestroy()
extern void Enemy_OnDestroy_mDCB05B4A7AE80D98E9F99470CAEFBB13F2BC6A94 (void);
// 0x0000001F System.Void Enemy::.ctor()
extern void Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734 (void);
// 0x00000020 System.Void EnemySpawner::Start()
extern void EnemySpawner_Start_m6B02CD71F904E1335A54E17E68D8BB2996347397 (void);
// 0x00000021 System.Void EnemySpawner::Update()
extern void EnemySpawner_Update_mE9B85A2C67A3E6E48ADC531A0426ECF999A4C2A3 (void);
// 0x00000022 System.Void EnemySpawner::SpawnNew()
extern void EnemySpawner_SpawnNew_mDF56F75D186498C083DC53950F45820BA209FDC0 (void);
// 0x00000023 System.Void EnemySpawner::IncrementKills()
extern void EnemySpawner_IncrementKills_m44B5B26E71B0A2655DE516DB142DE9725D7EC751 (void);
// 0x00000024 System.Void EnemySpawner::IncrementNotKilled()
extern void EnemySpawner_IncrementNotKilled_m49C8CB865FCCE535EB1426D87AB4688260A67DAA (void);
// 0x00000025 System.Boolean EnemySpawner::HasLost()
extern void EnemySpawner_HasLost_m44498D9FEE5FB8C36447341BC98BFB9AFADB3EE6 (void);
// 0x00000026 System.Void EnemySpawner::NotKilled()
extern void EnemySpawner_NotKilled_m24FA83DB39CDCD0E7FE3E8948E42A88650297664 (void);
// 0x00000027 System.Void EnemySpawner::.ctor()
extern void EnemySpawner__ctor_mED6FECD7E1057991ED710CD104501453BEEDA871 (void);
// 0x00000028 System.Void EnemySpawner::.cctor()
extern void EnemySpawner__cctor_m7D7D4050A703373F603657D9382C6E4FDB991ACC (void);
// 0x00000029 System.Void FatEnemy::Start()
extern void FatEnemy_Start_m9508C51841CF626F7B4B463861B0382251714BC9 (void);
// 0x0000002A System.Void FatEnemy::Update()
extern void FatEnemy_Update_mF6BDA098DDB9A751383F8F677BE1CCF73C3416E4 (void);
// 0x0000002B System.Void FatEnemy::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void FatEnemy_OnCollisionEnter2D_m40D9D4CD37EE32BB8895BAA8EBD238462FFAF1EB (void);
// 0x0000002C System.Void FatEnemy::OnCollisionExit2D(UnityEngine.Collision2D)
extern void FatEnemy_OnCollisionExit2D_m33F6007D576E4877DD784D8D8E64F0A51B4ABEE4 (void);
// 0x0000002D System.Void FatEnemy::Damage(System.Int32)
extern void FatEnemy_Damage_mE09903AF85C5C11BC32EF70020D53145581FC36B (void);
// 0x0000002E System.Void FatEnemy::.ctor()
extern void FatEnemy__ctor_m71DC17429ACA13A4A5E86FD98F8AAEB566428E1B (void);
// 0x0000002F System.Void Food::Start()
extern void Food_Start_m4198F3A63B32B95234570AA97E9D7F8C68420A45 (void);
// 0x00000030 System.Void Food::Update()
extern void Food_Update_mCA352900DC6368FA9431200E1BA19E3F24CC46A1 (void);
// 0x00000031 System.Void Food::FollowMouse()
extern void Food_FollowMouse_m59BDD62B5AA3609228D3EF1FB1400ABF6EDEBA20 (void);
// 0x00000032 System.Void Food::SetPosition(UnityEngine.Vector3,Cell)
extern void Food_SetPosition_m8EC2D0904C9EFA2830258BCABB4126CBA5B79ED7 (void);
// 0x00000033 System.Void Food::SetCell(Cell)
extern void Food_SetCell_m036623BEF9157A8FC5A6E49982F1C8267DCD3EB9 (void);
// 0x00000034 System.Void Food::StopMouseFollow()
extern void Food_StopMouseFollow_m14A65193D83AD970B69DE929707C76111F3EE80E (void);
// 0x00000035 System.Void Food::Shoot()
extern void Food_Shoot_m1B38A34409216343A1A8478CCD8E7FFCE2092E97 (void);
// 0x00000036 System.Void Food::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Food_OnCollisionEnter2D_m5494152478EC41B74BB436ADC7EF1994D8FD8F94 (void);
// 0x00000037 System.Boolean Food::OnCell()
extern void Food_OnCell_m7BCA77152F5C8654E427F6469E4FCEDC73536719 (void);
// 0x00000038 System.Void Food::RecieveDamage(System.Single)
extern void Food_RecieveDamage_m2109ADE73B51AC722B84AFF350DB74D1880B0EF7 (void);
// 0x00000039 System.Collections.IEnumerator Food::HotDog()
extern void Food_HotDog_m4E61255D3786CCD42228E9DFB3D901D0AF461C2E (void);
// 0x0000003A System.Void Food::OnDestroy()
extern void Food_OnDestroy_m8AB5009A690BA229206D793FAEFFEA7063BD15E5 (void);
// 0x0000003B System.Void Food::.ctor()
extern void Food__ctor_mD8BEBAF7DA65EACE3B649D043CBFB574337B57A3 (void);
// 0x0000003C System.Void Food/<HotDog>d__23::.ctor(System.Int32)
extern void U3CHotDogU3Ed__23__ctor_m217837BF4E86E22699BC2E37A2F35ECED073D257 (void);
// 0x0000003D System.Void Food/<HotDog>d__23::System.IDisposable.Dispose()
extern void U3CHotDogU3Ed__23_System_IDisposable_Dispose_m1B743D33808471B2615BF1F175F4EA6731AF165C (void);
// 0x0000003E System.Boolean Food/<HotDog>d__23::MoveNext()
extern void U3CHotDogU3Ed__23_MoveNext_mE5559A15C2B6D48F261B7056C8470A3921EE7C43 (void);
// 0x0000003F System.Object Food/<HotDog>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CHotDogU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBC9C5D29C4C12E255AB905143FFDBAFB3F5C1AE6 (void);
// 0x00000040 System.Void Food/<HotDog>d__23::System.Collections.IEnumerator.Reset()
extern void U3CHotDogU3Ed__23_System_Collections_IEnumerator_Reset_m2A6E27DCB189A0693C1D8038493428F7E5678DDA (void);
// 0x00000041 System.Object Food/<HotDog>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CHotDogU3Ed__23_System_Collections_IEnumerator_get_Current_mB56DD7A6DBF1893A3B73AFA05AC2F0005CB3B302 (void);
// 0x00000042 System.Void FoodSelector::Start()
extern void FoodSelector_Start_m2E22F57D27350C947873C81B654B7862EC1F881B (void);
// 0x00000043 System.Void FoodSelector::.ctor()
extern void FoodSelector__ctor_m279176D505C6FF19B65DA4BBCE5D915BFC09B780 (void);
// 0x00000044 System.Void FoodSelector::.cctor()
extern void FoodSelector__cctor_mE191C72FEC0E9C116C073EEF54E452017FDA30E4 (void);
// 0x00000045 System.Void FoodSelector/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m6444B1F3ABA4159265B0EA5FAE58745276B8DE84 (void);
// 0x00000046 System.Void FoodSelector/<>c__DisplayClass4_0::<Start>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CStartU3Eb__0_mF8C765E2A6C92C4B9EA559CE617B6CDFF8B1C31E (void);
// 0x00000047 System.Void HappySoda::Start()
extern void HappySoda_Start_mE69DD212C3E6A9A3421FA964CB858EBB170A3BB6 (void);
// 0x00000048 System.Void HappySoda::Update()
extern void HappySoda_Update_mAC1464D5B8C4F73F5E30A99BAEA0A073ACEEB37D (void);
// 0x00000049 System.Void HappySoda::.ctor()
extern void HappySoda__ctor_m15A5D04DF95A5C8F13A969C6B9E4EAFF68502632 (void);
// 0x0000004A System.Void HappySodaBullet::Start()
extern void HappySodaBullet_Start_m2E9F024965CAB5C4646F9C31B35E12B7DCB3953F (void);
// 0x0000004B System.Void HappySodaBullet::Update()
extern void HappySodaBullet_Update_mF51F0514D38D5866D5A04FC550AA6E891FDE647A (void);
// 0x0000004C System.Void HappySodaBullet::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void HappySodaBullet_OnCollisionEnter2D_m15C054E9F061E6F1CA489F7E6D89B0C74715BA95 (void);
// 0x0000004D System.Collections.IEnumerator HappySodaBullet::Explode()
extern void HappySodaBullet_Explode_mC2324FBACD2D6D30A3B1564D7750FB1E96C080DE (void);
// 0x0000004E System.Void HappySodaBullet::CheckLimit()
extern void HappySodaBullet_CheckLimit_mAFBB544BF193F6C6609F5002DA2EAD19F5A9A128 (void);
// 0x0000004F System.Void HappySodaBullet::.ctor()
extern void HappySodaBullet__ctor_m26026BD9A377BDFC92099ADCF0266267D5BC5ADE (void);
// 0x00000050 System.Void HappySodaBullet/<Explode>d__8::.ctor(System.Int32)
extern void U3CExplodeU3Ed__8__ctor_m6003CD66B2C3A80F7136D4B1D9789CCF62BCA98F (void);
// 0x00000051 System.Void HappySodaBullet/<Explode>d__8::System.IDisposable.Dispose()
extern void U3CExplodeU3Ed__8_System_IDisposable_Dispose_m795805C372B6ECA1BEE3DB0F890AC57B019FE7B2 (void);
// 0x00000052 System.Boolean HappySodaBullet/<Explode>d__8::MoveNext()
extern void U3CExplodeU3Ed__8_MoveNext_m75912C2B780AC3E81056BD59467156937086AB18 (void);
// 0x00000053 System.Object HappySodaBullet/<Explode>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExplodeU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF4BC4034B964815878A4841147B1D2701DCE530 (void);
// 0x00000054 System.Void HappySodaBullet/<Explode>d__8::System.Collections.IEnumerator.Reset()
extern void U3CExplodeU3Ed__8_System_Collections_IEnumerator_Reset_m5771317D158405D30CFD835AADD8AEC702A70BD8 (void);
// 0x00000055 System.Object HappySodaBullet/<Explode>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CExplodeU3Ed__8_System_Collections_IEnumerator_get_Current_mB7F5DBC9578AD2BA3F5D9806E9AD7F9DEAC8E504 (void);
// 0x00000056 System.Void HotDogBurstSFX::Start()
extern void HotDogBurstSFX_Start_mDE7EFF273D3727CF291DCA5C358D8FD7E648E9F0 (void);
// 0x00000057 System.Void HotDogBurstSFX::Play()
extern void HotDogBurstSFX_Play_m0297FF9D5B32185F85B4E23D6E5BF8D73391ACF3 (void);
// 0x00000058 System.Void HotDogBurstSFX::.ctor()
extern void HotDogBurstSFX__ctor_mB7AA97970EB69DC88044922C02F5A8A3C709B9DC (void);
// 0x00000059 System.Void MainMenuScript::Start()
extern void MainMenuScript_Start_m07DE3389F7C54C9B69C2D01391FA1DE37D550080 (void);
// 0x0000005A System.Void MainMenuScript::.ctor()
extern void MainMenuScript__ctor_m79A233ADFE2F6CBCAB273DBDE247C5644E682954 (void);
// 0x0000005B System.Void MainMenuScript/<>c::.cctor()
extern void U3CU3Ec__cctor_m400E17A63360AE36B21D5FA45D137999A149CC1E (void);
// 0x0000005C System.Void MainMenuScript/<>c::.ctor()
extern void U3CU3Ec__ctor_mB911AB5D0484BE8093CE6DC00727AA955C59079D (void);
// 0x0000005D System.Void MainMenuScript/<>c::<Start>b__3_0()
extern void U3CU3Ec_U3CStartU3Eb__3_0_mE90412217C9295F61BF5FD7C8485F5CB07DC1691 (void);
// 0x0000005E System.Void MainMenuScript/<>c::<Start>b__3_1()
extern void U3CU3Ec_U3CStartU3Eb__3_1_m6DF50902D4DA7302D32B307B83978D6C1713AE7F (void);
// 0x0000005F System.Void MeatBall::Start()
extern void MeatBall_Start_m12E583EF3DF75D5DF7C4895AF5F92D2ADD38D8AC (void);
// 0x00000060 System.Void MeatBall::Update()
extern void MeatBall_Update_mBBAEA35FA1FE12EC7EC1BCFB0E6B21DB77E0CC8D (void);
// 0x00000061 System.Void MeatBall::FollowMouse()
extern void MeatBall_FollowMouse_mDF32A3389ED1C6135995CCD0AAD9809BA835BDE0 (void);
// 0x00000062 System.Collections.IEnumerator MeatBall::Explode()
extern void MeatBall_Explode_m6A4159C50EC9A512988343D0C6FC53220B7C9572 (void);
// 0x00000063 System.Void MeatBall::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void MeatBall_OnCollisionEnter2D_m408697D61E8A3E9C73743F1E7D7EF0AEC8A79647 (void);
// 0x00000064 System.Void MeatBall::.ctor()
extern void MeatBall__ctor_m3FB755658CE3E4594D854CA4CA1B12D1E94EE78B (void);
// 0x00000065 System.Void MeatBall/<Explode>d__6::.ctor(System.Int32)
extern void U3CExplodeU3Ed__6__ctor_m15BBA8738DA3290CF7715DE5BD294C209ECFC8E1 (void);
// 0x00000066 System.Void MeatBall/<Explode>d__6::System.IDisposable.Dispose()
extern void U3CExplodeU3Ed__6_System_IDisposable_Dispose_m76D877688B5531DE217CA2B2869409B06163E1FD (void);
// 0x00000067 System.Boolean MeatBall/<Explode>d__6::MoveNext()
extern void U3CExplodeU3Ed__6_MoveNext_m0800400C810670A4183EDC5100B1859A812E2B8F (void);
// 0x00000068 System.Object MeatBall/<Explode>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExplodeU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFF90C3DA5AF102776CCB2B0338C2C7D70F600D07 (void);
// 0x00000069 System.Void MeatBall/<Explode>d__6::System.Collections.IEnumerator.Reset()
extern void U3CExplodeU3Ed__6_System_Collections_IEnumerator_Reset_mA28EFE71ECF08D2AE84331A34A8E64986B25D242 (void);
// 0x0000006A System.Object MeatBall/<Explode>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CExplodeU3Ed__6_System_Collections_IEnumerator_get_Current_mD6AC28EB25702D522FEA4AA077A3AED03B805CBF (void);
// 0x0000006B System.Void MeatBallExplosionSFX::Start()
extern void MeatBallExplosionSFX_Start_m8E08B94FDF4B19D8CD31FC02ED90DBDCFEF3DA21 (void);
// 0x0000006C System.Void MeatBallExplosionSFX::Play()
extern void MeatBallExplosionSFX_Play_m4B8094C2C05B7BC20A65615A487E9E8F7D1375B7 (void);
// 0x0000006D System.Void MeatBallExplosionSFX::SFXReset()
extern void MeatBallExplosionSFX_SFXReset_m67921B571CA10007CEDC6A7274F2A12E4B18EE73 (void);
// 0x0000006E System.Void MeatBallExplosionSFX::.ctor()
extern void MeatBallExplosionSFX__ctor_m2B6EAEF16ECA25809869970D9EEE3870C7D82ED0 (void);
// 0x0000006F System.Void Money::Update()
extern void Money_Update_m275C3CB3FFA573396790F3FF2BF1E4DF176791D0 (void);
// 0x00000070 System.Void Money::OnMouseOver()
extern void Money_OnMouseOver_m76340D795CCF44C7C47723420D6A082900136136 (void);
// 0x00000071 System.Void Money::.ctor()
extern void Money__ctor_m64A54A25D43DCC49A0B405A7C343B4FA38575C44 (void);
// 0x00000072 System.Int32 MoneyCounter::get_Amount()
extern void MoneyCounter_get_Amount_mAFE8D371F7BF1E79D1DAE73E8033CAB9D9546930 (void);
// 0x00000073 System.Void MoneyCounter::set_Amount(System.Int32)
extern void MoneyCounter_set_Amount_mFDB3F03DE8774B506A348AB89CE45D419385C1D3 (void);
// 0x00000074 System.Void MoneyCounter::Start()
extern void MoneyCounter_Start_mAA5D8063EB5D17431E1D63000A6FFDDA665C9261 (void);
// 0x00000075 System.Void MoneyCounter::IncrementAmount(System.Int32)
extern void MoneyCounter_IncrementAmount_mDC84B1F987167E97F0D6099F5CD3E61CD7BBE6B9 (void);
// 0x00000076 System.Void MoneyCounter::UpdateText()
extern void MoneyCounter_UpdateText_m61848ACBAF91B2ADF362B0D3629E6DEFA2AAD1CA (void);
// 0x00000077 System.Void MoneyCounter::DecrementAmount(System.Int32)
extern void MoneyCounter_DecrementAmount_m12DF7B7CBAD7563C09E0C83CE9A16EFBE4ACBE3C (void);
// 0x00000078 System.Void MoneyCounter::.ctor()
extern void MoneyCounter__ctor_m34E7C358F03FFAEC9D027DE0266894EA8449D234 (void);
// 0x00000079 System.Void MoneySpawner::Start()
extern void MoneySpawner_Start_m76B05A5D2FBC2718FDCD085B2D74A6BCD5BA0A2D (void);
// 0x0000007A System.Single MoneySpawner::RandomAdInterval()
extern void MoneySpawner_RandomAdInterval_m8F2175975E2666DA9D0BC69723FE05570C0FC7F3 (void);
// 0x0000007B System.Void MoneySpawner::Update()
extern void MoneySpawner_Update_m9C29556968F56F2AC7523A2463B1059CBD460D67 (void);
// 0x0000007C System.Void MoneySpawner::SetRandomInterval()
extern void MoneySpawner_SetRandomInterval_m20DADD8366B3B020A0E71A5A36BB8EDA87F923B3 (void);
// 0x0000007D System.Single MoneySpawner::RandomX()
extern void MoneySpawner_RandomX_mC8705FD849CE4B159AD153BFC276165E76E43E19 (void);
// 0x0000007E System.Void MoneySpawner::SpawnAd()
extern void MoneySpawner_SpawnAd_m65F76CC349142186F4DF5EA9E281C9027A25A7D7 (void);
// 0x0000007F System.Void MoneySpawner::.ctor()
extern void MoneySpawner__ctor_m786F3D7CB05EDF9DE01A3F0E205E037B8F28056D (void);
// 0x00000080 System.Void ObjectLimter::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void ObjectLimter_OnCollisionEnter2D_mDD443BE1207A2FFF3CE9867187DAD2D7A63CE8A6 (void);
// 0x00000081 System.Void ObjectLimter::.ctor()
extern void ObjectLimter__ctor_m40FF6F2F62B7AFC0D52E0DFD6F2C0DA3676F4430 (void);
// 0x00000082 System.Void SplashBulletSFX::Start()
extern void SplashBulletSFX_Start_mFC505A02DDC4901F974915F10FE1B417F80D1BD9 (void);
// 0x00000083 System.Void SplashBulletSFX::Play()
extern void SplashBulletSFX_Play_mFCF0E2DC394657E4D5BA40B2C239C2C806DD0D6D (void);
// 0x00000084 System.Void SplashBulletSFX::.ctor()
extern void SplashBulletSFX__ctor_mA8D70A4D2AEDB4830FC46CACA0E73CD94F69E212 (void);
static Il2CppMethodPointer s_methodPointers[132] = 
{
	Ad_Update_m0737678DBC5B201E41569F013BD1968FEFD4105C,
	Ad_OnMouseOver_mB67E9B63528F82639A78327B5B8AC41CD822FC2A,
	Ad__ctor_m382BABF025DD59677C46BE975AD2EFFFFB17D786,
	BtnUIAnim_Start_mE630C13F932121054C3FCAB33C64845147A9D24F,
	BtnUIAnim_OnPointerEnter_m5031E9AD336BBD24E0A326D5C22190F7FF8FA60C,
	BtnUIAnim_OnPointerExit_m0957D203470A7C1892C29D0D00FFC621F2A62B48,
	BtnUIAnim__ctor_m57A995316CBBA22870E9B2EFDDC8207CE2EF2DC7,
	Bullet_Start_m58181B46F80FE1ABECE1AFF455C253B51B7EB0E4,
	Bullet_Update_mB82408CA535D8533168045E7C0321090448B596B,
	Bullet_OnCollisionEnter2D_m4CC80D190EE3A6B6E2EDA524A121942D0285D0A9,
	Bullet_CheckLimit_mC03B03994B3FD61DF42471BB4514A70A7288228D,
	Bullet__ctor_mC7D931FE508342F413FBA79525A4819D4114B3EC,
	BulletCollisionSFX_Start_m0F3D065B83D2B3449725A1A0C0F8C62B066C597D,
	BulletCollisionSFX_Play_mCAB4D2BE4E57B52999154A100B16DFDCC033E84D,
	BulletCollisionSFX__ctor_mF5347294466952AEFD874945638D93B74D93C1CD,
	Cell_OnTriggerStay2D_m6CDC37C2ABD61860B2D4539AA8031DA05946DEE7,
	Cell_Disoccupy_mD6FA209271B12606A6AD2A562EBA1EE8D2383C24,
	Cell__ctor_m8A64B8FBB74A0ECF5EE0871E80CD28E644CBAAF7,
	EatingSFXPlayer_Start_m82E2B0033416D89F79771443DA7CFB28D080E01E,
	EatingSFXPlayer_PlayEatingClip_m201E93CA7C16BB956C05CAA3A1C9CA3E710FF576,
	EatingSFXPlayer__ctor_m7D428A4DD85451C5ACC663C54F9585B5F09A97A9,
	Enemy_Start_m9FA35B427F2B9FDFD390E9812C2556775C62CB02,
	Enemy_Update_mA01EE7AF5D3B97687752E9D22BECB4A3E13F8FD2,
	Enemy_OnCollisionEnter2D_m47EAF0D1D60EC5BF3953975EA6BF15189DF82A12,
	Enemy_OnCollisionExit2D_m90E6DC91A08B3861C3E2A6440553466DAE158E1B,
	Enemy_SetEatAnim_m31342088BDB7E2AF7C9A010A5E92284034E1228B,
	Enemy_Damage_m56D6962DD19E23B5E086C2F6CB4D097E1CA61F19,
	Enemy_CheckLimit_m74A309FB85B77A5407AB2698FE153A720F5B5BEE,
	Enemy_DoEatingSoundClip_mB1FCECC6982E1CE7B331C92C6EE9B3DED9FBC53D,
	Enemy_OnDestroy_mDCB05B4A7AE80D98E9F99470CAEFBB13F2BC6A94,
	Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734,
	EnemySpawner_Start_m6B02CD71F904E1335A54E17E68D8BB2996347397,
	EnemySpawner_Update_mE9B85A2C67A3E6E48ADC531A0426ECF999A4C2A3,
	EnemySpawner_SpawnNew_mDF56F75D186498C083DC53950F45820BA209FDC0,
	EnemySpawner_IncrementKills_m44B5B26E71B0A2655DE516DB142DE9725D7EC751,
	EnemySpawner_IncrementNotKilled_m49C8CB865FCCE535EB1426D87AB4688260A67DAA,
	EnemySpawner_HasLost_m44498D9FEE5FB8C36447341BC98BFB9AFADB3EE6,
	EnemySpawner_NotKilled_m24FA83DB39CDCD0E7FE3E8948E42A88650297664,
	EnemySpawner__ctor_mED6FECD7E1057991ED710CD104501453BEEDA871,
	EnemySpawner__cctor_m7D7D4050A703373F603657D9382C6E4FDB991ACC,
	FatEnemy_Start_m9508C51841CF626F7B4B463861B0382251714BC9,
	FatEnemy_Update_mF6BDA098DDB9A751383F8F677BE1CCF73C3416E4,
	FatEnemy_OnCollisionEnter2D_m40D9D4CD37EE32BB8895BAA8EBD238462FFAF1EB,
	FatEnemy_OnCollisionExit2D_m33F6007D576E4877DD784D8D8E64F0A51B4ABEE4,
	FatEnemy_Damage_mE09903AF85C5C11BC32EF70020D53145581FC36B,
	FatEnemy__ctor_m71DC17429ACA13A4A5E86FD98F8AAEB566428E1B,
	Food_Start_m4198F3A63B32B95234570AA97E9D7F8C68420A45,
	Food_Update_mCA352900DC6368FA9431200E1BA19E3F24CC46A1,
	Food_FollowMouse_m59BDD62B5AA3609228D3EF1FB1400ABF6EDEBA20,
	Food_SetPosition_m8EC2D0904C9EFA2830258BCABB4126CBA5B79ED7,
	Food_SetCell_m036623BEF9157A8FC5A6E49982F1C8267DCD3EB9,
	Food_StopMouseFollow_m14A65193D83AD970B69DE929707C76111F3EE80E,
	Food_Shoot_m1B38A34409216343A1A8478CCD8E7FFCE2092E97,
	Food_OnCollisionEnter2D_m5494152478EC41B74BB436ADC7EF1994D8FD8F94,
	Food_OnCell_m7BCA77152F5C8654E427F6469E4FCEDC73536719,
	Food_RecieveDamage_m2109ADE73B51AC722B84AFF350DB74D1880B0EF7,
	Food_HotDog_m4E61255D3786CCD42228E9DFB3D901D0AF461C2E,
	Food_OnDestroy_m8AB5009A690BA229206D793FAEFFEA7063BD15E5,
	Food__ctor_mD8BEBAF7DA65EACE3B649D043CBFB574337B57A3,
	U3CHotDogU3Ed__23__ctor_m217837BF4E86E22699BC2E37A2F35ECED073D257,
	U3CHotDogU3Ed__23_System_IDisposable_Dispose_m1B743D33808471B2615BF1F175F4EA6731AF165C,
	U3CHotDogU3Ed__23_MoveNext_mE5559A15C2B6D48F261B7056C8470A3921EE7C43,
	U3CHotDogU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBC9C5D29C4C12E255AB905143FFDBAFB3F5C1AE6,
	U3CHotDogU3Ed__23_System_Collections_IEnumerator_Reset_m2A6E27DCB189A0693C1D8038493428F7E5678DDA,
	U3CHotDogU3Ed__23_System_Collections_IEnumerator_get_Current_mB56DD7A6DBF1893A3B73AFA05AC2F0005CB3B302,
	FoodSelector_Start_m2E22F57D27350C947873C81B654B7862EC1F881B,
	FoodSelector__ctor_m279176D505C6FF19B65DA4BBCE5D915BFC09B780,
	FoodSelector__cctor_mE191C72FEC0E9C116C073EEF54E452017FDA30E4,
	U3CU3Ec__DisplayClass4_0__ctor_m6444B1F3ABA4159265B0EA5FAE58745276B8DE84,
	U3CU3Ec__DisplayClass4_0_U3CStartU3Eb__0_mF8C765E2A6C92C4B9EA559CE617B6CDFF8B1C31E,
	HappySoda_Start_mE69DD212C3E6A9A3421FA964CB858EBB170A3BB6,
	HappySoda_Update_mAC1464D5B8C4F73F5E30A99BAEA0A073ACEEB37D,
	HappySoda__ctor_m15A5D04DF95A5C8F13A969C6B9E4EAFF68502632,
	HappySodaBullet_Start_m2E9F024965CAB5C4646F9C31B35E12B7DCB3953F,
	HappySodaBullet_Update_mF51F0514D38D5866D5A04FC550AA6E891FDE647A,
	HappySodaBullet_OnCollisionEnter2D_m15C054E9F061E6F1CA489F7E6D89B0C74715BA95,
	HappySodaBullet_Explode_mC2324FBACD2D6D30A3B1564D7750FB1E96C080DE,
	HappySodaBullet_CheckLimit_mAFBB544BF193F6C6609F5002DA2EAD19F5A9A128,
	HappySodaBullet__ctor_m26026BD9A377BDFC92099ADCF0266267D5BC5ADE,
	U3CExplodeU3Ed__8__ctor_m6003CD66B2C3A80F7136D4B1D9789CCF62BCA98F,
	U3CExplodeU3Ed__8_System_IDisposable_Dispose_m795805C372B6ECA1BEE3DB0F890AC57B019FE7B2,
	U3CExplodeU3Ed__8_MoveNext_m75912C2B780AC3E81056BD59467156937086AB18,
	U3CExplodeU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF4BC4034B964815878A4841147B1D2701DCE530,
	U3CExplodeU3Ed__8_System_Collections_IEnumerator_Reset_m5771317D158405D30CFD835AADD8AEC702A70BD8,
	U3CExplodeU3Ed__8_System_Collections_IEnumerator_get_Current_mB7F5DBC9578AD2BA3F5D9806E9AD7F9DEAC8E504,
	HotDogBurstSFX_Start_mDE7EFF273D3727CF291DCA5C358D8FD7E648E9F0,
	HotDogBurstSFX_Play_m0297FF9D5B32185F85B4E23D6E5BF8D73391ACF3,
	HotDogBurstSFX__ctor_mB7AA97970EB69DC88044922C02F5A8A3C709B9DC,
	MainMenuScript_Start_m07DE3389F7C54C9B69C2D01391FA1DE37D550080,
	MainMenuScript__ctor_m79A233ADFE2F6CBCAB273DBDE247C5644E682954,
	U3CU3Ec__cctor_m400E17A63360AE36B21D5FA45D137999A149CC1E,
	U3CU3Ec__ctor_mB911AB5D0484BE8093CE6DC00727AA955C59079D,
	U3CU3Ec_U3CStartU3Eb__3_0_mE90412217C9295F61BF5FD7C8485F5CB07DC1691,
	U3CU3Ec_U3CStartU3Eb__3_1_m6DF50902D4DA7302D32B307B83978D6C1713AE7F,
	MeatBall_Start_m12E583EF3DF75D5DF7C4895AF5F92D2ADD38D8AC,
	MeatBall_Update_mBBAEA35FA1FE12EC7EC1BCFB0E6B21DB77E0CC8D,
	MeatBall_FollowMouse_mDF32A3389ED1C6135995CCD0AAD9809BA835BDE0,
	MeatBall_Explode_m6A4159C50EC9A512988343D0C6FC53220B7C9572,
	MeatBall_OnCollisionEnter2D_m408697D61E8A3E9C73743F1E7D7EF0AEC8A79647,
	MeatBall__ctor_m3FB755658CE3E4594D854CA4CA1B12D1E94EE78B,
	U3CExplodeU3Ed__6__ctor_m15BBA8738DA3290CF7715DE5BD294C209ECFC8E1,
	U3CExplodeU3Ed__6_System_IDisposable_Dispose_m76D877688B5531DE217CA2B2869409B06163E1FD,
	U3CExplodeU3Ed__6_MoveNext_m0800400C810670A4183EDC5100B1859A812E2B8F,
	U3CExplodeU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFF90C3DA5AF102776CCB2B0338C2C7D70F600D07,
	U3CExplodeU3Ed__6_System_Collections_IEnumerator_Reset_mA28EFE71ECF08D2AE84331A34A8E64986B25D242,
	U3CExplodeU3Ed__6_System_Collections_IEnumerator_get_Current_mD6AC28EB25702D522FEA4AA077A3AED03B805CBF,
	MeatBallExplosionSFX_Start_m8E08B94FDF4B19D8CD31FC02ED90DBDCFEF3DA21,
	MeatBallExplosionSFX_Play_m4B8094C2C05B7BC20A65615A487E9E8F7D1375B7,
	MeatBallExplosionSFX_SFXReset_m67921B571CA10007CEDC6A7274F2A12E4B18EE73,
	MeatBallExplosionSFX__ctor_m2B6EAEF16ECA25809869970D9EEE3870C7D82ED0,
	Money_Update_m275C3CB3FFA573396790F3FF2BF1E4DF176791D0,
	Money_OnMouseOver_m76340D795CCF44C7C47723420D6A082900136136,
	Money__ctor_m64A54A25D43DCC49A0B405A7C343B4FA38575C44,
	MoneyCounter_get_Amount_mAFE8D371F7BF1E79D1DAE73E8033CAB9D9546930,
	MoneyCounter_set_Amount_mFDB3F03DE8774B506A348AB89CE45D419385C1D3,
	MoneyCounter_Start_mAA5D8063EB5D17431E1D63000A6FFDDA665C9261,
	MoneyCounter_IncrementAmount_mDC84B1F987167E97F0D6099F5CD3E61CD7BBE6B9,
	MoneyCounter_UpdateText_m61848ACBAF91B2ADF362B0D3629E6DEFA2AAD1CA,
	MoneyCounter_DecrementAmount_m12DF7B7CBAD7563C09E0C83CE9A16EFBE4ACBE3C,
	MoneyCounter__ctor_m34E7C358F03FFAEC9D027DE0266894EA8449D234,
	MoneySpawner_Start_m76B05A5D2FBC2718FDCD085B2D74A6BCD5BA0A2D,
	MoneySpawner_RandomAdInterval_m8F2175975E2666DA9D0BC69723FE05570C0FC7F3,
	MoneySpawner_Update_m9C29556968F56F2AC7523A2463B1059CBD460D67,
	MoneySpawner_SetRandomInterval_m20DADD8366B3B020A0E71A5A36BB8EDA87F923B3,
	MoneySpawner_RandomX_mC8705FD849CE4B159AD153BFC276165E76E43E19,
	MoneySpawner_SpawnAd_m65F76CC349142186F4DF5EA9E281C9027A25A7D7,
	MoneySpawner__ctor_m786F3D7CB05EDF9DE01A3F0E205E037B8F28056D,
	ObjectLimter_OnCollisionEnter2D_mDD443BE1207A2FFF3CE9867187DAD2D7A63CE8A6,
	ObjectLimter__ctor_m40FF6F2F62B7AFC0D52E0DFD6F2C0DA3676F4430,
	SplashBulletSFX_Start_mFC505A02DDC4901F974915F10FE1B417F80D1BD9,
	SplashBulletSFX_Play_mFCF0E2DC394657E4D5BA40B2C239C2C806DD0D6D,
	SplashBulletSFX__ctor_mA8D70A4D2AEDB4830FC46CACA0E73CD94F69E212,
};
static const int32_t s_InvokerIndices[132] = 
{
	1151,
	1151,
	1151,
	1151,
	985,
	985,
	1151,
	1151,
	1151,
	985,
	1151,
	1151,
	1151,
	1151,
	1151,
	985,
	1151,
	1151,
	1151,
	985,
	1151,
	1151,
	1151,
	985,
	985,
	954,
	976,
	1151,
	1151,
	1151,
	1151,
	1151,
	1151,
	1151,
	1151,
	1151,
	1976,
	1151,
	1151,
	2001,
	1151,
	1151,
	985,
	985,
	976,
	1151,
	1151,
	1151,
	1151,
	658,
	985,
	1151,
	1151,
	985,
	1089,
	1003,
	1117,
	1151,
	1151,
	976,
	1151,
	1089,
	1117,
	1151,
	1117,
	1151,
	1151,
	2001,
	1151,
	1151,
	1151,
	1151,
	1151,
	1151,
	1151,
	985,
	1117,
	1151,
	1151,
	976,
	1151,
	1089,
	1117,
	1151,
	1117,
	1151,
	1151,
	1151,
	1151,
	1151,
	2001,
	1151,
	1151,
	1151,
	1151,
	1151,
	1151,
	1117,
	985,
	1151,
	976,
	1151,
	1089,
	1117,
	1151,
	1117,
	1151,
	1151,
	1151,
	1151,
	1151,
	1151,
	1151,
	1106,
	976,
	1151,
	976,
	1151,
	976,
	1151,
	1151,
	1138,
	1151,
	1151,
	1138,
	1151,
	1151,
	985,
	1151,
	1151,
	1151,
	1151,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	132,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
